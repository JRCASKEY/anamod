"""Top-level package for anamod."""

__author__ = """Akshay Sood"""
__email__ = "sood.iitd@gmail.com"
__version__ = "0.1.0"


from anamod.model_analyzer import ModelAnalyzer  # noqa: F401
